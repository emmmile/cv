\ProvidesClass{friggeri-cv}[2012/04/30 CV class]
\NeedsTeXFormat{LaTeX2e}

\DeclareOption{print}{\def\@cv@print{}}
\DeclareOption*{%
  \PassOptionsToClass{\CurrentOption}{article}%
}
\ProcessOptions\relax
\LoadClass{article}

%%%%%%%%%%
% Colors %
%%%%%%%%%%

\RequirePackage{xcolor}
\RequirePackage{longtable}

\definecolor{white}{RGB}{255,255,255}

\definecolor{darkgray}{HTML}{222222}
\definecolor{gray}{HTML}{3D3D3D}
\definecolor{lightgray}{HTML}{999999}

\definecolor{green}{HTML}{C2E15F}
\definecolor{orange}{HTML}{FDA333}
\definecolor{purple}{HTML}{D3A4F9}
\definecolor{red}{HTML}{FB4485}
\definecolor{blue}{HTML}{6CE0F1}

\ifdefined\@cv@print
  \colorlet{green}{gray}
  \colorlet{orange}{gray}
  \colorlet{purple}{gray}
  \colorlet{brown}{gray}
  \colorlet{red}{gray}
  \colorlet{blue}{gray}
  \colorlet{fillheader}{white}
  \colorlet{header}{gray}

  \colorlet{textcolor}{black}
  \colorlet{headercolor}{black}
\else
  \colorlet{fillheader}{gray}
  \colorlet{header}{white}
  \colorlet{textcolor}{gray}
  \colorlet{headercolor}{gray}
\fi

%%%%%%%%%
% Fonts %
%%%%%%%%%

\RequirePackage[]{fontspec}
% \RequirePackage[math-style=TeX,vargreek-shape=unicode]{unicode-math}
\RequirePackage{unicode-math}



\newfontfamily\bodyfont[
  Path = ./template/ ,
  UprightFont=*,
  ItalicFont=*-Italic,
  BoldFont=*-Bold,
  BoldItalicFont=*-BoldItalic
]{HelveticaNeue}

\newfontfamily\thinfont[
  Path = ./template/ ,
  UprightFont=*,
  ItalicFont=*Italic
]{HelveticaNeue-UltraLight}

\newfontfamily\headingfont[
  Path = ./template/ ,
  UprightFont=*
]{HelveticaNeue-CondensedBold}

\defaultfontfeatures{Mapping=tex-text}
\setmainfont[
  Mapping=tex-text,
  Path = ./template/ ,
  UprightFont=*,
  ItalicFont=*Italic
]{HelveticaNeue-Light}


%\newfontfamily\bodyfont[]{Helvetica Neue}
%\newfontfamily\thinfont[]{Helvetica Neue UltraLight}
%\newfontfamily\headingfont[]{Helvetica Neue Condensed Bold}

%\defaultfontfeatures{Mapping=tex-text}
%\setmainfont[Mapping=tex-text, Color=textcolor]{Helvetica Neue Light}


%\setmathfont{XITS Math}

%%%%%%%%%%
% Header %
%%%%%%%%%%

\RequirePackage{tikz}

\newcommand{\rolefont}{%
  \fontsize{14pt}{24pt}\selectfont%
  \thinfont%
  \color{white}%
}

\newcommand{\header}[2]{%
  \begin{tikzpicture}[remember picture,overlay]
    \node [rectangle, fill=fillheader, anchor=north, minimum width=\paperwidth, minimum height=4cm] (box) at (current page.north){};
    \node [anchor=center] (name) at (box) {%
      \fontsize{40pt}{72pt}\color{header}%
      {\headingfont #1}{\thinfont #2}{}
    };
  \end{tikzpicture}
  \vspace{2.5cm}
  \vspace{-2\parskip}
}

\newcommand{\headerwithtitle}[3]{%
  \begin{tikzpicture}[remember picture,overlay]
    \node [rectangle, fill=fillheader, anchor=north, minimum width=\paperwidth, minimum height=4cm] (box) at (current page.north){};
    \node [anchor=center] (name) at (box) {%
      \fontsize{40pt}{72pt}\color{header}%
      {\thinfont #1}{\bodyfont #2}{}
    };
    \node [anchor=north] at (name.south) {%
      \fontsize{14pt}{24pt}\color{header}%
      \thinfont #3%
    };
  \end{tikzpicture}
  \vspace{2.5cm}
  \vspace{-2\parskip}
}


%%%%%%%%%%%%%
% Structure %
%%%%%%%%%%%%%
\RequirePackage{parskip}

\newcounter{colorCounter}
\def\@sectioncolor#1#2#3{%
  {%
    \color{%
      \ifcase\value{colorCounter}%
        orange\or%
        blue\or%
        red\or%
        green\or%
        purple\or%
        brown\else%
        headercolor\fi%
    } #1#2#3%
  }%
  \stepcounter{colorCounter}%
}

\newcounter{importantCounter}
\newcommand{\importantcolor}[1]{%
  {%
    \color{%
      \ifcase\value{importantCounter}%
        red\or%
        orange\or%
        green\or%
        purple\or%
        blue\or%
        brown\else%
        headercolor\fi%
    }{#1}%
  }%
  \stepcounter{importantCounter}%
}

\newcommand{\important}[1]{\textbf{\importantcolor{#1}}}

\renewcommand{\section}[1]{
  \par\vspace{\parskip}
  {%
    \LARGE\headingfont\color{headercolor}%
    \@sectioncolor #1%
  }
  \par\vspace{\parskip}
}

\renewcommand{\subsection}[2]{
  \par\vspace{.5\parskip}%
  \Large\headingfont\color{headercolor} #2%
  \par\vspace{.25\parskip}%
}

\pagestyle{empty}


%%%%%%%%%%%%%%%%%%%%
% List environment %
%%%%%%%%%%%%%%%%%%%%

\setlength{\tabcolsep}{0pt}
\newenvironment{entrylist}{%
  \begin{tabular*}{\textwidth}{@{\extracolsep{\fill}}ll}
}{%
  \end{tabular*}
}
\renewcommand{\bfseries}{\headingfont\color{headercolor}}
\newcommand{\entry}[4]{%
  #1&\parbox[t]{11.4cm}{%
    \textbf{#2}%
    \hfill%
    {\footnotesize\addfontfeature{Color=gray} #3}\\%
    #4\vspace{\parsep}%
  }\\}
\newcommand{\entrywithposition}[5]{%
  #1&\parbox[t]{11.4cm}{%
    \textbf{#2}%
    \hfill%
    {\footnotesize\addfontfeature{Color=gray} #3}\\%
    {\bfseries\addfontfeature{Color=gray} #4}\\%
    #5\vspace{\parsep}%
  }\\}
% 1: date
% 2: company
% 3: location
% 4: title
% 5: description
% 6: tags
\newcommand{\newentry}[6]{%
  \parbox[t]{\textwidth}{%
    \textbf{#2}%
    \hfill%
    {\footnotesize\addfontfeature{Color=gray} #3}\\%
    {\bfseries\addfontfeature{Color=gray} #4}%
    \hfill%
    {\footnotesize\addfontfeature{Color=gray} #1}\\%
    #5%
    \ifx#6\empty%
      % Tags are empty
    \else%
      {\footnotesize\addfontfeature{Color=gray} #6}%
    \fi\vspace{\parsep}%
  }
}

% 1: date
% 2: title
% 3: description
% 4: tags
\newcommand{\samecompany}[4]{%
  \parbox[t]{\textwidth}{%
    {\bfseries\addfontfeature{Color=gray} #2}%
    \hfill%
    {\footnotesize\addfontfeature{Color=gray} #1}\\%
    #3%
    \ifx#4\empty%
      % Tags are empty
    \else%
      {\footnotesize\addfontfeature{Color=gray} #4}%
    \fi\vspace{\parsep}%
  }
}


%%%%%%%%%%%%%%
% Side block %
%%%%%%%%%%%%%%

\RequirePackage[absolute,overlay]{textpos}
\setlength{\TPHorizModule}{1cm}
\setlength{\TPVertModule}{1cm}
\newenvironment{aside}{%
  \let\oldsection\section
  \renewcommand{\section}[1]{
    \par\vspace{\baselineskip}{\Large\headingfont\color{headercolor} ##1}
  }
  \begin{textblock}{4.1}(1.5, 4.33)
  \begin{flushright}
  \obeycr
}{%
  \restorecr
  \end{flushright}
  \end{textblock}
  \let\section\oldsection
}

\newenvironment{asidecoverletter}{%
  \let\oldsection\section
  \renewcommand{\section}[1]{
    \par\vspace{\baselineskip}{\Large\headingfont\color{headercolor} ##1}
  }
  \begin{textblock}{4.1}(1.5, 4.5)
  \begin{flushright}
  \obeycr
}{%
  \restorecr
  \end{flushright}
  \end{textblock}
  \let\section\oldsection
}

%%%%%%%%%%%%%%%%
% Bibliography %
%%%%%%%%%%%%%%%%

\RequirePackage[style=verbose, maxnames=99, sorting=ydnt, backend=biber]{biblatex}

\DeclareFieldFormat[article]{title}{#1\par}
\DeclareFieldFormat[book]{title}{#1\par}
\DeclareFieldFormat[inproceedings]{title}{#1\par}
\DeclareFieldFormat[misc]{title}{#1\par}
\DeclareFieldFormat[report]{title}{#1\par}

\DeclareBibliographyDriver{article}{%
  \printfield{title}%
  \newblock%
  \printnames{author}%
  \par%
  \newblock%
  {%
    \footnotesize\addfontfeature{Color=lightgray}\itshape%
    \usebibmacro{journal+issuetitle}%
    \setunit{\space}%
    \printfield{pages}%
    \newunit%
    \printlist{publisher}%
    \setunit*{\addcomma\space}%
    \printfield{year}%
    \newunit%
  }
  \par\vspace{0.3\baselineskip}
}

\DeclareBibliographyDriver{book}{%
  \printfield{title}%
  \newblock%
  \printnames{author}%
  \par%
  \newblock%
  {%
    \footnotesize\addfontfeature{Color=lightgray}\itshape%
    \printlist{publisher}%
    \setunit*{\addcomma\space}%
    \printfield{note}%
    \setunit*{\addcomma\space}%
    \printfield{year}%
    \setunit{\addcomma\space}%
    \printlist{location}%
    \newunit%
  }
  \par\vspace{0.3\baselineskip}
}


\DeclareBibliographyDriver{inproceedings}{%
  \printfield{title}%
  \newblock%
  \printnames{author}%
  \par%
  \newblock%
  {%
    \footnotesize\addfontfeature{Color=lightgray}%
    \printfield{booktitle}%
    \setunit{\addcomma\space}%
    \printfield{year}%
    \setunit{\addcomma\space}%
    \printlist{location}%
    \newunit%
  }
  \par\vspace{0.3\baselineskip}
}

\DeclareBibliographyDriver{misc}{%
  \printfield{title}%
  \newblock%
  \printnames{author}%
  \par%
  \newblock%
  {%
    \footnotesize\addfontfeature{Color=lightgray}\itshape%
    \printfield{booktitle}%
    \setunit*{\addcomma\space}%
    \printfield{note}%
    \setunit*{\addcomma\space}%
    \printfield{year}%
    \setunit{\addcomma\space}%
    \printlist{location}%
    \newunit%
  }
  \par\vspace{0.3\baselineskip}
}

\DeclareBibliographyDriver{report}{%
  \printfield{title}%
  \newblock%
  \printnames{author}%
  \par%
  \newblock%
  {%
    \footnotesize\addfontfeature{Color=lightgray}\itshape%
    \printfield{type}%
    \setunit{\space}%
    \printfield{number}%
    \setunit{\addcomma\space}%
    \printfield{year}%
    \newunit%
  }
  \par\vspace{0.3\baselineskip}
}

\DeclareNameFormat{author}{%
  \small\addfontfeature{Color=lightgray}%
  \ifgiveninits
    {\usebibmacro{name:given-family}
      {\namepartfamily}
      {\namepartgiveni}
      {\namepartprefix}
      {\namepartsuffix}}
    {\usebibmacro{name:given-family}
      {\namepartfamily}
      {\namepartgiven}
      {\namepartprefix}
      {\namepartsuffix}}%
  \usebibmacro{name:andothers}}

\newcommand{\printbibsection}[2]{%
  \begin{refsection}%
    \newrefcontext[sorting=chronological]%
    \nocite{*}%
    \printbibliography[type={#1}, title={#2}, heading=subbibliography]%
  \end{refsection}}

\DeclareSortingScheme{chronological}{
  \sort[direction=descending]{\field{year}}
  \sort[direction=descending]{\field{month}}
}



%%%%%%%%%%%%%%%%
% Other tweaks %
%%%%%%%%%%%%%%%%

\RequirePackage[left=6.1cm,top=2cm,right=1.5cm,bottom=2.5cm,nohead,nofoot]{geometry}
\RequirePackage{hyperref}


\ifdefined\@cv@print
  \hypersetup{%
    colorlinks=false,% hyperlinks will be black
    linkbordercolor=white,% hyperlink borders will be red
    pdfborderstyle={/S/U/W 0}% border style will be underline of width 1pt
  }%
\else
  \hypersetup{%
    colorlinks=false,% hyperlinks will be orange
    % urlcolor=orange,
    linkbordercolor=black,% hyperlink borders will be red
    pdfborderstyle={/S/U/W 1}% border style will be underline of width 1pt
  }%
\fi


\newcommand{\plus}{\raisebox{.2\height}{\scalebox{.8}{+}}}
\newcommand{\minus}{\raisebox{.2\height}{\scalebox{.8}{-}}}
\newcommand\cplusplus{C\plus\plus}
