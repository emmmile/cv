[This](https://gitlab.com/emmmile/cv/-/jobs/artifacts/master/raw/emiliodeltessandoro.pdf?job=pdf) is latest my curriculum vitae, in PDF format.

# How the heck this works?

The CV is built with `scons` and uses `xelatex` internally. The CV itself is based on the famous Friggeri template.

The CV is built at every push on `master` via Gitlab CI/CD. An [artifact](https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html) is created as a result of that.

# Other cool CVs to look at in the future

* https://www.sharelatex.com/templates/cv-or-resume/fancy-cv
* https://github.com/posquit0/Awesome-CV
* https://luisfuentes.gitlab.io/resume/


# How to run locally

Install Docker, then build the image. This will download the whole latex image, which is about 3Gb in size:

```bash
docker build --tag "xelatex" .
```

Then run it as an action container. This mounts the `industrial` cv into the directory `/cv` in the container, and then run `scons` on that directory.

```bash
docker run --rm -v $(pwd)/industrial:/cv xelatex -C /cv
```