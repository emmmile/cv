FROM aergus/latex:latest

RUN apt-get install scons

ENTRYPOINT ["scons"]
